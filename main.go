package main // import "gitlab.com/sh4869221/echo_test"

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/sh4869221/echo_test/controller"
)

func main() {
	e := echo.New()
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "${time} method=${method}, uri=${uri}, status=${status}, remote=${remote_ip}\n",
	}))
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})
	e.GET("/list", controller.GetList())
	e.Logger.Fatal(e.Start(":1323"))
}
