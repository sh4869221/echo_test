module gitlab.com/sh4869221/echo_test

go 1.12

require (
	github.com/labstack/echo/v4 v4.1.14
	github.com/mattn/go-isatty v0.0.12 // indirect
	golang.org/x/crypto v0.0.0-20200221231518-2aa609cf4a9d // indirect
	golang.org/x/net v0.0.0-20200222033325-078779b8f2d8 // indirect
	golang.org/x/sys v0.0.0-20200219091948-cb0a6d8edb6c // indirect
)
